require "libs/rect"

sprite = rect:prototype()
function sprite:collides(other) return self:intersects(other) or other:intersects(self) end
table.setreadonly(sprite)

config = setmetatable({
	audio = {
		explosion = "resources/audio/Explosion.wav",
		lightening = "resources/audio/Lightening.wav",
		mercury = "resources/audio/Mercury.wav",
		shoot = "resources/audio/Shoot.wav"
	},
	torpedoTubes = 4,
	ground = rect:prototype {
		width = 400,
		height = 25,
		sprite = "resources/images/ground.png"
	},
	gameOver = "resources/images/game_over.png",
	submarine = sprite:prototype {
		width = 64,
		height = 32,
		speed = 200,
		sprite = "resources/images/submarine.png",
		maxSpeed = 400,
		ignored = false,
	},
	torpedo = sprite:prototype {
		width = 16,
		height = 8,
		speed = 100,
		sprite = "resources/images/torpedo.png",
		loadTime = 1.6,
		maxSpeed = 3200,
		torpedoCollision = true,
	},
	enemy = sprite:prototype {
		width = 64,
		height = 32,
		speed = 150,
		sprite = "resources/images/shark.png",
		chargeDistance = 100,
		attackAngle = 50,
		canCharge = true,
		maxSpeed = 1000,
		chargeSpeed = 400,
	},
	spawnTimer = object:prototype {
		min = 0.1,
		max = 2,
		decrease = 0.02,
		interval = 3
	},
}, {
	__index = function(self, key)
		local enemies = {
			squid = self.enemy:prototype {
				speed = 150,
				sprite = "resources/images/squid.png",
				canCharge = false,
				attackAngle = 80
			},
			shark = self.enemy:prototype {
				canCharge = false,
				speed = 300,
				attackAngle = 30
			},
			swordfish = self.enemy:prototype {
				speed = 200,
				sprite = "resources/images/swordfish.png",
				canCharge = true,
				chargeDistance = 150,
				chargeSpeed = 500,
				attackAngle = 0,
			},
		}

		if key == "squid" then return enemies.squid
		elseif key == "shark" then return enemies.shark
		elseif key == "swordfish" then return enemies.swordfish
		elseif key == "enemyTypes" then return table.keys(enemies)
		else error("Invalid key") end
	end
})

table.setreadonly(config)