local updateMovement = function(submarine)
	local dSpeed = subby._.dTime * subby.submarine.speed
	-- slower speed on x/y combo
	if from({
		subby._.request.move.down or subby._.request.move.up,
		subby._.request.move.left or subby._.request.move.right
	}):all(function(b) return b end) then dSpeed = dSpeed / math.sqrt(2) end
	-- up/down
	if subby._.request.move.down and subby.submarine.position.y < subby._.canvas.height - subby.submarine.height then
		subby.submarine.position.y = subby.submarine.position.y + dSpeed
		subby.submarine.movement["down"] = true
	elseif subby._.request.move.up and subby.submarine.position.y > 0 then
		subby.submarine.position.y = subby.submarine.position.y - dSpeed
		subby.submarine.movement["up"] = true
	end
	-- left/right
	if subby._.request.move.right and subby.submarine.position.x < subby._.canvas.width - subby.submarine.width then
		subby.submarine.position.x = subby.submarine.position.x + dSpeed
		subby.submarine.movement["right"] = true
	elseif subby._.request.move.left and subby.submarine.position.x > 0 then
		subby.submarine.position.x = subby.submarine.position.x - dSpeed
		subby.submarine.movement["left"] = true
	end
end

local updateTorpedoes = function(submarine)

	local moveTorpedo = function(torpedo, ti)
		torpedo.position.x = torpedo.position.x + subby._.dTime * torpedo.speed
		-- reach max speed at end
		if torpedo.speed < torpedo.maxSpeed then
			torpedo.speed = torpedo.speed + subby._.dTime * (torpedo.maxSpeed - torpedo.speed)
		end
		-- remove when out of sight
		if torpedo.position.x > subby._.canvas.width then
			table.remove(subby.torpedoBay.torpedoes, ti)
			subby.score.misses = subby.score.misses + 1
		end
	end

	local sendTorpedo = function(key)
		-- check tube
		if subby.torpedoBay.tubes[key] > 0 then return
		else subby.torpedoBay.tubes[key] = config.torpedo.loadTime end
		-- release
		local momentum = 0
		if subby.submarine.movement.left then momentum = -subby.submarine.speed / 2
		elseif subby.submarine.movement.right then momentum = subby.submarine.speed / 2 end
		local torpedo = config.torpedo:prototype {
			speed = config.torpedo.speed + momentum,
			position = {
				x = subby.submarine.position.x + subby.submarine.width,
				y = subby.submarine.position.y + subby.submarine.height / 2
			}
		}
		local shoot = function()
			table.insert(subby.torpedoBay.torpedoes, torpedo)
			subby.event.shoot = true
		end
		if torpedo.torpedoCollision then
			local lastTorpedo = subby.torpedoBay.torpedoes[#subby.torpedoBay.torpedoes]
			if not (lastTorpedo and torpedo:collides(lastTorpedo)) then shoot()
			else subby.event.explosion = true end
		else shoot() end
	end

	for ti in pairs(subby.torpedoBay.tubes) do subby.torpedoBay.tubes[ti] = subby.torpedoBay.tubes[ti] - subby._.dTime end
	from(subby.torpedoBay.torpedoes):each(moveTorpedo)
	from(subby._.request.torpedo)
		:select(function(pressed, key) return { key = key, pressed = pressed } end)
		:where(function(button, key) return button.pressed end)
		:select(function(button) return button.key end)
		:each(sendTorpedo)
end

submarine = {}

function submarine:update()
	updateMovement(self)
	updateTorpedoes(self)
end

table.setreadonly(submarine)