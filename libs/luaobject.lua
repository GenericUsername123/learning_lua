require "libs/extensions"

object = {}

--- Shallow clone an object.
-- @return the cloned object
function object:clone() return table.join({}, self) end

--- Prototype using metatables.
-- @return the new object
-- function object:prototype(args)
function object:prototype(init) return setmetatable(table.assert(init, "init"), { __index = self }) end

--- Prototype only the fields of an object (no metatable fields).
-- @param init initialization table
-- @return the new object
function object:shallowPrototype(init) return table.join(self:clone(), table.assert(init, "init")) end

--- Sets the __call field to a prototype call.
-- @param isShallow
-- @return self
function object:setprototype(isShallow)
	return setmetatable(self, table.join(getmetatable(self), { __call = function(this, init)
		return isShallow and self:shallowPrototype(this, init) or self:prototype(this, init)
	end }))
end

table.setreadonly(object)