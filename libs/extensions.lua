require "libs/lualinq"

-- #region string

function string:split(delimiter)
	local result = {}
	local from = 1
	local delim_from, delim_to = self.find(self, delimiter, from)
	while delim_from do
		table.insert(result, self.sub(self, from, delim_from-1))
		from = delim_to + 1
		delim_from, delim_to = self.find(self, delimiter, from)
	end
	table.insert(result, self.sub(self, from))
	return result
end

-- #endregion

-- #region table

--- Assert if the table is of correct type.
-- @param arg the argument to check
-- @param name the argument name
-- @return the argument (initialized if nil)
function table.assert(arg, name)
	arg = arg or {}
	assert(type(arg) == "table", "argument "..name.." is of invalid type")
	return arg
end

--- Join two tables.
-- @param self the main table
-- @param other the other table
-- @return the main table joined with the other
function table.join(self, other)
	self = table.assert(self, "self")
	other = table.assert(other, "other")
	from(other):foreach(function(o) self[o.key] = o.value end)
	return self
end

function table.setreadonly(self) return setmetatable(self, table.join(getmetatable(self), {
	__newindex = function() error("this table is readonly") end
})) end

function table.keys(self) return from(self):select(function(o) return o.key end):toArray() end
function table.values(self) return from(self):select(function(o) return o.value end):toArray() end

--- Generate a new table.
--@param size the size of the table
--@param values the values callback (optional)
--@param keys the keys callback (optional)
--@return the generated table
function table.new(size, values, keys)
	keys = keys or function(i) return i end
	values = values or function(i) return i end
	if values then assert(type(values) == "function", "fakeLinq: invalid parameter type") end
	if keys then assert(type(keys) == "function", "fakeLinq: invalid parameter type") end
	local collection = {}
	for i=1,size do collection[keys(i)] = values(i) end
	return collection
end

-- #endregion