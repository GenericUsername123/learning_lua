-- Tutorial: https://www.tutorialspoint.com/lua/index.htm

--[[

Basic syntax

--]]

print("Hello world, from ",_VERSION,"!\n")

local var = false
if var then
	print("var == true")
else
	print("var == false")
end
var = not var
print("var == " .. tostring(var))

--[[

Variables

--]]

local a,b = 1,2
local c,d = "Test", 5
print("a + b = " .. 1+2)
print(c .. d)
print("(int / int) a / b = " .. a/b)
a,b = 15, 12
print("a = " .. a)

--[[

Data types & operators

--]]

local uninit
if not uninit then
	print("not nil == true")
end

print("\nTypes table:")
-- can't do this: print("="*99)
print("uninit:", type(uninit))
print("a:", type(a))
print("var:", type(var))
print("c (".. c .."):", type(c))
print("")
-- ~= is !=
print("a ~= b", a ~= b)
print("Length of c", #c)

--[[

Loops & decision making

]]--

print("while loop...")
a = 3
b = a
while a >= 0 do
	print(b-a)
	a = a -1
end
print("until loop...")
a = b
repeat
	print(b-a)
	a = a -1
until a < 0
print("for loop...")
for i=0,b do
	print(i)
end
-- also proves: numbers are by value
-- verifying if strings are by reference
local str1 = "value"
local str2 = str1
str1 = "reference"
print("Strings are by " .. str2)
-- break also works
for i=1,10 do
	if i > 5 then
		print("I don't like CLI spamming")
		break -- here
	elseif not (i < 5) then print(i) end
end

--[[

Functions

]]--

local function do_thing(thing) return "Did thing " .. thing end

local function do_things(start, end_, callback)
	print("Lemme do some things real quick...")
	callback = callback or do_thing
	for i=start,end_ do print(callback(i)) end
	print("Didn't I look busy?")
end

do_things(1, 3)
do_things(1, 3, function() return "YEET" end)
--[[
Thing to note:
* invoking a function with a parameters whilst that function has no parameters defined works.
* functions operate the same way as they do in JS
]]--

-- apparently for i = start,end,>increment< exists
for i = 1, 2, .5 do print(i) end

--[[

Strings

]]--

str1 = [[Let me "Qoute" this and see if str1 is stored as str1]]
print(str1)
-- \a is like printf "\a"
print(string.rep("=", 80)) -- what I previously tried with "="*99
print("Title")
print(string.rep("=", 80))
print("ascii-nr (for T):",string.byte("T"))
print("Float of 4 after decimal:", string.format("pi = %.4f", math.pi))
local d,m,y = 16,06,2018
print("Date of writing line 131:",string.format("%02d/%02d/%04d", d, m, y))
local waldo = "Find Waldo"
-- also demonstrates start & end index
print("Waldo is @ (start\tend)", string.find(waldo, "Waldo", 1, #waldo-1))
local nemo = string.gsub(string.gsub(waldo, "Waldo", "Nemo"), "Find", "Finding")
print(waldo .. " is now " .. nemo)
--[[
I didn't take my time to look at this, but I'll get back to this.
]]--
local s = "hello world from Lua"
for w in string.gmatch(s, "%a+") do print(w) end
local t = {}
s = "from=world, to=Lua"
for k, v in string.gmatch(s, "(%w+)=(%w+)") do t[k] = v end
print("hello " .. t["from"])
local search_string = string.lower("Can giraffes scream?")
local keyword = " "
if not string.match(search_string, "can") then keyword = "not " end
print("Can is " .. keyword .. "in the search string")

--[[

Arrays

]]--

local a1 = {1,4,2,6,3,3}
print(a1[1]) -- startindex != 0 but = 1
print(a1[-1]) -- not last index (array = dict)

-- check if by reference
local a2 = a1
a1 = {}
print(#a1 .. #a2)

a1 = {}
local maxRows, maxCols = 3,3
for row=0,maxRows-1 do
	a1[row] = {}
	for col=0,maxCols-1 do
		a1[row][col] = tonumber(row..col)
		print(a1[row][col])
	end
end
a2 = {
	{0,1,2},
	{10,11,12},
	{20,21,22},
}
for row=1,maxRows do -- startindex != 0 but = 1
	for col=1,maxCols do
		print(a2[row][col])
	end
end

--[[

Iterators

]]--

a1 = {"Elephant", "Lion", "Giraffe", "Zebra"}
for k,v in ipairs(a1) do
	print(k,v)
end

local function square(iteratorMaxCount,currentNumber)
	if currentNumber<iteratorMaxCount
	then
		currentNumber = currentNumber+1
		return currentNumber, currentNumber*currentNumber
	end
end
--[[
for k,v in func square with args : (3,0)

the last arg increases by 1 every iteration, this increase is stored outside the function
(i.e. what started with argument 2 as 0 is 3 after 3 iterations)

square returns k: arg2 & v: arg2*arg2
]]--
for i,n in square,3,0 do print(i,n) end

local function squares(max) return square,max, 0 end -- note: func,args == iterator, func(args) == number
for k,v in squares(3) do print(k,v) end

--[[
Closures: the previous example has only an iterator returning func,args,
closures are functions those iterators return using previously defined variables
(not passed, but from scope, e.g. lambda accessing outside variables)
]]--
local function elementIterator (collection)
   local index = 0 -- would otherwise be an arg (same for count)
   local count = #collection
   return function ()
      index = index + 1
      if index <= count then return collection[index] end
   end
end
for element in elementIterator(a1) do print(element) end

--[[

Tables

]]--

local function print_words(words)
	print(table.concat(words, " "))
end

-- concat & insert/remove
a1 = {"hello", "world"}
print_words(a1)
table.insert(a1,2,"cruel")
table.remove(a1,1)
table.insert(a1,1,"farewell")
print_words(a1)

-- sort
a1 = {1,4,2,6,3,3}
table.sort(a1)
print(table.concat(a1, ","))

--[[

Modules

]]--

local testModule = require("tutorial/module")
testModule.test()

--[[

Metatables

]]--

print(testModule.meta) -- test __index
testModule.new = "new value"
print(testModule.new) -- test __newindex
local newModule = setmetatable({test=""}, getmetatable(testModule))
print(testModule == newModule)
newModule.test = testModule.test
print(testModule == newModule) -- test __eq
print(testModule) -- test __tostring

--[[

Coroutines

]]--

-- local routine = coroutine.create(function(value)
-- 	print
-- end)

co = coroutine.create(function (value1,value2)
	local tempvar3 = 10
	print("coroutine section 1", value1, value2, tempvar3)

	local tempvar1 = coroutine.yield(value1+1,value2+1) -- only one param stored
	tempvar3 = tempvar3 + value1
	print("coroutine section 2",tempvar1 ,tempvar2, tempvar3)

	local tempvar1, tempvar2= coroutine.yield(value1+value2, value1-value2) -- 2 params stored
	tempvar3 = tempvar3 + value1
	print("coroutine section 3",tempvar1,tempvar2, tempvar3)

	return value2, "end" -- end routine
end)

print("main", coroutine.resume(co, 3, 2))
print("main", coroutine.resume(co, 12, nil)) -- only one params is read
print("main", coroutine.resume(co, 5, 6)) -- last instructions
print("main", coroutine.resume(co, 10)) -- routine is dead

local function getNumber()
	local function getNumberHelper()
		local routine = coroutine.create(function ()
			coroutine.yield(1)
			coroutine.yield(2)
			coroutine.yield(3)
			coroutine.yield(4)
			coroutine.yield(5)
		end)
		return routine
	end
	if(numberHelper) then
		local status, number = coroutine.resume(numberHelper);
		if coroutine.status(numberHelper) == "dead" then
			numberHelper = getNumberHelper()
			status, number = coroutine.resume(numberHelper);
		end
		return number
	else
		numberHelper = getNumberHelper()
		local status, number = coroutine.resume(numberHelper);
		return number
	end
end

local runRoutine = function()
	local getRoutine = function() return coroutine.create(function(start) coroutine.yield(coroutine.yield(start)) end) end
	if (routine) then
		local status, value = coroutine.resume(routine, "pong")
		if coroutine.status(routine) == "dead" then -- it needs to run before checking status
			routine = getRoutine()
			status, value = coroutine.resume(routine, "ping")
		end
		return value
	else
		routine = getRoutine()
		local status, value = coroutine.resume(routine, "ping")
		return value
	end
end

for i = 1, 10 do print(i, getNumber()) end
for i = 1, 5 do print(runRoutine()) end

--[[

File I/O
tables & optional parameters from tutorial:
https://www.tutorialspoint.com/lua/lua_file_io.htm

]]--

local file = io.open("routine.log", "a")
io.output(file)
io.write("[signal] signal start\n")
for i = 1, 5 do io.write("[signal:"..i.."] "..runRoutine().."\n") end
io.write("[signal] signal stop\n")
io.close(file)

file = io.open("routine.log", "r")
io.input(file)
print(io.read())
print(io.read())
for ln in io.lines() do print(ln) end
io.close(file)

--[[

Error handling

]]--

local minMaxScaler = function(collection)
	local getValues = function(collection)
		local index = 0
		return function()
			index = index+1
			if index <= #collection then return collection[index] end
		 end
	end

	assert(type(collection) == "table", "collection is not a table")
	local min,max = math.min(table.unpack(collection)), math.max(table.unpack(collection))
	local newCollection = {}
	for value in getValues(collection) do
		if value < 0 then value = value/-min
		else value = value/max end
		table.insert(newCollection, #newCollection+1, value)
	end
	return newCollection
end

local ds, status = {-1,-4,2,3,4,5}
status, ds = pcall(minMaxScaler, ds)
if status then print(table.unpack(ds)) else print("Error") end
status = xpcall(minMaxScaler, function() print("Error") end)
print(status)

--[[

Not in tutorial

]]--

print(table.concat({
	string.rep("=", 80),
	"Testing lualinq",
	string.rep("=", 80)
}, "\n"))

require "libs/lualinq"
local testCase = {1,2,3,4,-5}
testCase = from(testCase):where(function(o) return o > 0 end):toArray() -- switch this on/off
print("Dataset:", table.unpack(testCase))
if from(testCase):all(function(o) return o > 0 end) then print("All above 1")
else print("Not all above 1") end
if from(testCase):any(function(o) return o < 0 end) then print("Any under 1")
else print("Not any under 1") end
local first = from(testCase):first(function(o, k) return o < k end, "not found")
print("First value = " .. first)
from(testCase):skip(2):each(print)
from(testCase):take(2):each(print)

local mr = {
	down = true,
	up = false,
	left = false,
	right = true
}
if from(mr):any(function(item) return item end) then print("Button pressed")
else print("No button pressed") end

local rt = {false, true, true, false}
from(rt):select(function(pressed, key) return {key=key, pressed=pressed} end)
	:where(function(item) return item["pressed"] end)
	:select(function(item) return "Requested torpedo nr. "..item.key end)
	:each(function(msg) print(msg) end)
xpcall(function() from(testCase):any() end, function(err) print("Query failed:", err) end)

print(table.concat({
	string.rep("=", 80),
	"Testing ternary operators",
	string.rep("=", 80)
}, "\n"))

-- ternary operators are possible
mr = {}
print("speed for none = true", (mr.left and -1 or (mr.right and 1 or 0)) * 100)
mr = {left=true}
print("speed for left = true", (mr.left and -1 or (mr.right and 1 or 0)) * 100)
mr = {right=true}
print("speed for right = true", (mr.left and -1 or (mr.right and 1 or 0)) * 100)

print(table.concat({
	string.rep("=", 80),
	"Testing luaobject",
	string.rep("=", 80)
}, "\n"))

print("=== prototype ===")
require "libs/luaobject"
local person = object:prototype { say = "rights before duties" }
local elephantPerson = person:prototype { say = "meta inherited", sad = true }
print(table.concat({
	"object.say="..tostring(object.say),
	"person.say="..tostring(person.say),
	"elephantPerson.say="..tostring(elephantPerson.say),
	"person.sad="..tostring(person.sad),
	"elephantPerson.sad="..tostring(elephantPerson.sad),
}, "\n"))
elephantPerson.key = "key" -- objects are readonly, but its children are not
print("=== prototype (no metatables) ===")
person = object:shallowPrototype { say = "rights before duties" }
elephantPerson = person:shallowPrototype { say = "meta inherited", sad = true }
print(table.concat({
	"object.say="..tostring(object.say),
	"person.say="..tostring(person.say),
	"elephantPerson.say="..tostring(elephantPerson.say),
	"person.sad="..tostring(person.sad),
	"elephantPerson.sad="..tostring(elephantPerson.sad),
}, "\n"))
elephantPerson.key = "key"

print(table.concat({
	string.rep("=", 80),
	"Closing session",
	string.rep("=", 80)
}, "\n"))
-- close session
if from(arg):any(function(item) return item == "remove-log" end) then
	os.remove("routine.log")
	print("Log removed...")
end