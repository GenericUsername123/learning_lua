# Learning Lua: tutorial

**Source: <https://www.tutorialspoint.com/lua/>**

- [x] **Lua basics tutorial**
  - [x] Basic syntax
  - [x] Variables
  - [x] Data types
  - [x] Operators
  - [x] Loops
  - [x] Decision making
  - [x] Functions
  - [x] Strings
  - [x] Arrays
  - [x] Iterators
  - [x] Tables
  - [x] Modules
  - [x] Metatables
  - [x]] Coroutines
  - [x] File I/O
  - [x] Error Handling