local logic = {
	test = function () print("YEET") end
}
return setmetatable(logic, {
	__index = function(table, key)
		if key == "meta" then return "meta"
		else return table[key] end
	end,
	__newindex = function(table, key, value)
		rawset(table, key, "["..value.."]")
	end,
	__eq = function(table, new)
		return table.test == new.test -- in this example only test matters in a comparison
	end,
	__tostring = function(table) return "test: "..tostring(table.test) end
})